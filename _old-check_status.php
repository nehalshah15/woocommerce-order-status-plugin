<?php
/*
 * Plugin Name: Woocommerce - check stock status
 * Plugin URI: http://prismitsystems.com
 * Description: This plugin helps you to display stock status of products in order page.
 * Version: 0.0.1
 * Author: Prism IT Systems
 * Author URI: http://prismitsystems.com
 */

function status_plugin_enqueue_script() {   
    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style('custom', plugins_url( '/css/custom.css', __FILE__ ));
}
add_action('admin_enqueue_scripts', 'status_plugin_enqueue_script');

register_activation_hook( __FILE__, 'create_db_real_stock' );
function create_db_real_stock() {
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'real_stock';

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		order_id smallint(5) NOT NULL,
		product_id smallint(5) NOT NULL,
		total_real_stock smallint(5) NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	maybe_create_table( $table_name, $sql );
}

function wc_new_order_column($columns)
{
	$new_columns = array();
    foreach ($columns as $column_name => $column_info) {
        $new_columns[$column_name] = $column_info;
        if ('order_total' === $column_name) {
            $new_columns['check_status'] = __('Stock','woocommerce');
        }
    }
    return $new_columns;
}
add_filter('manage_edit-shop_order_columns','wc_new_order_column');

add_action('woocommerce_checkout_order_processed','set_backorder_status',11);
//add_action('init','set_backorder_status');
function set_backorder_status($order_id)
{	
	global $wpdb;
	$order = wc_get_order( $order_id ); 
	
	foreach ($order->get_items() as $item_id => $item) {
		$o_item_id = $item_id;
		$variation_id = $item->get_variation_id();
		$backorder_status = get_post_meta($variation_id,'_backorders',true);
		$product_id = $item->get_product_id();
		$backorder_status_simple = get_post_meta($product_id,'_backorders',true);
						
		if($backorder_status == 'yes' || $backorder_status == 'notify' || $backorder_status_simple == 'yes' || $backorder_status_simple == 'notify')
		{
			$var_stock    = get_post_meta($variation_id,'_stock',true);
			$simple_stock = get_post_meta($product_id,'_stock',true);
			
			$product_qty  = $item->get_quantity();		
			
			$product = wc_get_product ( $product_id );					
			if( $product->is_type( 'variable' ) )
			{			
				if( $var_stock < $product_qty )
				{
					wc_update_order_item_meta($o_item_id,'backorder_status','1');
				}
				
				$total_real_stock_variable = $product_qty + $var_stock;
				$wpdb->insert( $wpdb->prefix . 'real_stock', 
					array( 
						'order_id'  => $order_id,
						'product_id' => $product_id,
						'total_real_stock'  => $total_real_stock_variable
					), 
					array( 
						'%d',
						'%d',
						'%d'
					) 
				);
			}	
			else
			{
				if( $simple_stock < $product_qty)
				{
					wc_update_order_item_meta($o_item_id,'backorder_status','1');
				}
				
				$total_real_stock_simple = $product_qty + $simple_stock;
				$wpdb->insert( $wpdb->prefix . 'real_stock', 
					array( 
						'order_id'  => $order_id,
						'product_id' => $product_id,
						'total_real_stock'  => $total_real_stock_simple
					), 
					array( 
						'%d',
						'%d',
						'%d'
					) 
				);
			}			
		}
	}	
}

add_filter('woocommerce_order_item_get_formatted_meta_data', 'hide_backorder_status');
function hide_backorder_status($formatted_meta)
{
	foreach($formatted_meta as $key=>$meta)
	{
		if($meta->key == 'backorder_status')
		{
			unset($formatted_meta[$key]);
		}
	}
	return $formatted_meta;
}

function wc_order_extra_columns_content($column)
{
	global $post, $wpdb,$product;
	
	$order_id = $post->ID;
	
	switch ($column)
	{
		case "check_status":
					
			 $order = new WC_Order($order_id);
			 $bool = true;
			 foreach ($order->get_items() as $order_item_id => $items) {
				 $backorder_status = wc_get_order_item_meta($order_item_id,'backorder_status',true);
				 if($backorder_status)
				 {
					 $bool = false;
				 }
				
			 }
			 
			 $order_status = $order->get_status();    
			 if ('processing' == $order_status) {  
				 
				 if($bool)
				 {
					$out_of_stock = false;
				 }
				 else
				 {				 				 
					 $last_order_date = $order->get_date_created();
					 			 
					 $orders_ids = $wpdb->get_results('
							SELECT DISTINCT WO.order_id
							FROM `wp_woocommerce_order_itemmeta` AS a
							LEFT JOIN `wp_woocommerce_order_itemmeta` AS b ON ( a.order_item_id = b.order_item_id )
							LEFT JOIN `wp_woocommerce_order_itemmeta` AS c ON ( b.order_item_id = c.order_item_id )
							LEFT JOIN `wp_woocommerce_order_items` AS WO ON ( WO.order_item_id = c.order_item_id )
							LEFT JOIN `wp_posts` AS p ON ( p.ID = WO.order_id )
							WHERE a.meta_key = "backorder_status" AND a.meta_value = 1 
							AND b.meta_key = "_product_id"
							AND p.post_date <= "'.$last_order_date.'"
							AND p.post_status = "wc-processing"
						');
						/*echo '<pre>';
						print_r($orders_ids);
						echo '</pre>';*/
						$arr = array();
						foreach($orders_ids as $ids)
						{
							$arr[] = $ids->order_id;
						}
						
						$product_stock_arr = array();
						foreach($arr as $id)
						{
							 $backorder = wc_get_order( $id );
							 $items = $backorder->get_items();
													 
							 foreach ( $items as $key=>$item ) {
									
								$product_name = $item->get_name();
								$product_id   = $item->get_product_id();
								
								$product_qty  = $item->get_quantity();
								$variation_id = $item->get_variation_id();
								$var_stock 	  = get_post_meta($variation_id,'_stock',true);
								$simple_stock = get_post_meta($product_id,'_stock',true);
																					
								$product = wc_get_product ( $product_id );
								
								if( $product->is_type( 'variable' ) )
								{
									$temp_qty = 0;
									if(isset($product_stock_arr[$variation_id]))
									{
										$temp_qty = $product_stock_arr[$variation_id];
									}
									$product_stock_arr[$variation_id] = $product_qty + $temp_qty;	
									
								}
								else
								{
									$temp_qty = 0;
									if(isset($product_stock_arr[$product_id]))
									{
										$temp_qty = $product_stock_arr[$product_id];
									}
									$product_stock_arr[$product_id] = $product_qty + $temp_qty;
									
								}
							}
						}
					
						 $items = $order->get_items();
						 
						 $out_of_stock = false;
						 foreach ( $items as $key=>$item ) {
							$order_item_id = $key;
							$product_name = $item->get_name();
							$product_id   = $item->get_product_id();
							$product_qty  = $item->get_quantity();
							$variation_id = $item->get_variation_id();
							if( !$product_id ){
								$out_of_stock = true;
								break;
							}	
							
							echo $total_real_stock = $wpdb->get_var("select total_real_stock from ".$wpdb->prefix."real_stock where order_id =".$order_id." and product_id =".$product_id);
														
							$product = wc_get_product( $product_id );
							if( $product->is_type( 'variable' ) )
							{
								$stock = get_post_meta($variation_id,'_stock',true);
								
								if(isset($product_stock_arr[$variation_id]))
								{
									if($stock < $product_stock_arr[$variation_id])
									{
										$out_of_stock = true;
									}
								}								
							}
							else
							{
								$stock = get_post_meta($product_id,'_stock',true);
								
								if(isset($product_stock_arr[$product_id]))
								{
									if($stock < $product_stock_arr[$product_id])
									{
										$out_of_stock = true;
									}
								}								
							}
						}	
					}
						
					if(!$out_of_stock)
					{
						echo '<i class="fa fa-check" style="font-size:30px;color:green" data-toggle="tooltip" data-placement="bottom" title="In Stock"></i>';
					}
					else
					{
						echo '<i class="fa fa-remove" style="font-size:30px;color:red" data-toggle="tooltip" data-placement="bottom" title="Out Of Stock"></i>';
					}
				}
				elseif('completed' == $order_status)
				{
					echo '<i class="fa fa-check" style="font-size:30px;color:green" data-toggle="tooltip" data-placement="bottom" title="In Stock"></i>';
				}
								
			break;	
	}
}
add_action('manage_posts_custom_column', 'wc_order_extra_columns_content');

add_action( 'woocommerce_product_options_inventory_product_data', 'woocom_inventory_product_data_custom_field' );
function woocom_inventory_product_data_custom_field() {
	global $post;
	
	$product = wc_get_product( $post->ID );
	if( $product->is_type( 'simple' ) )
	{
		  woocommerce_wp_text_input( 
			array( 
			  'id' => 'simple_real_stock', 
			  'label' => __( 'Real Stock', 'woocommerce' ), 
			  'desc_tip' => 'true',
			  //'value' => get_post_meta($post->ID, '', true),
			  'disabled' => 'disabled' 
			)
		  );
		  ?>
		  <script type="text/javascript">
				jQuery(document).ready(function(){
					  jQuery("#simple_real_stock").attr("readonly","readonly");
				});
		  </script>
  <?php
	}
}

add_action( 'woocommerce_product_after_variable_attributes', 'variable_fields', 10, 3 );

function variable_fields( $loop, $variation_data, $variation ) {
	
	global $post;
	
?>
	<tr>
		<td>
			<?php
			
				woocommerce_wp_text_input( 
					array( 
						'id'          => 'variable_real_stock['.$loop.']', 
						'label'       => __( 'Real Stock', 'woocommerce' ), 
						'desc_tip'    => 'true',
						//'value'       => get_post_meta($variation->ID, '', true),
						'style' 	  => 'pointer-events: none;'
					)
				);
			
			?>
			
		</td>
	</tr>
<?php
}
