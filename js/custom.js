jQuery(document).ready(function(){
	if( !jQuery('.order_lists').length ){
		return;
	}
	var physical_stock = [];
	var product_status = JSON.parse( jQuery('#product_status').val() );
	jQuery('.order_lists tbody tr:last-child td').each(function(index){
		if( index > 1 ){
			physical_stock[ index ] = jQuery(this).html();
		}
	});
	
	jQuery('.order_lists tbody tr').each(function(rowIndex){
		if( jQuery(this).find('.order_status_mark').length ){
			var hasStock = 1;
			var hasStock2 = 0;
			var hasStock1 = 0;
			jQuery(this).find('td').each(function(index){
				if( index > 1 ){
					var current_product_qty = parseInt( jQuery(this).html() );
					if( parseInt(current_product_qty) > 0 ){
						if( physical_stock[ index ] >= current_product_qty ){
							physical_stock[ index ] = parseInt( physical_stock[ index ] ) - current_product_qty;		
						}else{
								hasStock = 2;	
						}
						
						if( current_product_qty && product_status[ index - 2 ] == "0" ){
							hasStock1 = 1;		
						}
						if( current_product_qty && product_status[ index - 2 ] == "1" ){
							hasStock2 = 1;		
						}
					}
				}	
			});
			if( hasStock2 ){
				hasStock = 3;			
			}
			if( hasStock1 ){
				hasStock = 4;			
			}
			switch( hasStock ){
				case 1:
					jQuery(this).find('.order_status_mark').addClass('has_stock');	
					break;
				case 2:
					jQuery(this).find('.order_status_mark').addClass('out_of_stock');	
					break;
				case 3:
					jQuery(this).find('.order_status_mark').addClass('product_without_track_in_stock');	
					break;
				case 4:
					jQuery(this).find('.order_status_mark').addClass('product_without_track_out_of_stock');	
					break;
			}
		}
	});
	
});
