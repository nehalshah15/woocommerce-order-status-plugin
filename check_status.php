<?php
/*
 * Plugin Name: Woocommerce - check stock status 2.0
 * Plugin URI: http://prismitsystems.com
 * Description: This plugin helps you to display stock status of products in order page.
 * Version: 0.0.1
 * Author: Prism IT Systems
 * Author URI: http://prismitsystems.com
 */

function status_plugin_enqueue_script2() {   
    wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style('custom2', plugins_url( '/css/custom.css', __FILE__ ));
    wp_enqueue_script('custom-js2', plugins_url( '/js/custom.js', __FILE__ ));
}
add_action('admin_enqueue_scripts', 'status_plugin_enqueue_script2');

function os_custom_menu_page2() {
    add_menu_page(
        __( 'Order Status', 'textdomain' ),
        'Order Status',
        'manage_options',
        'order_status_page',
        'order_status_table_callback',
        '',
        58
    );
}
add_action( 'admin_menu', 'os_custom_menu_page2' );

function order_status_table_callback()
{
	include_once "order_status_list.php";
	echo '<div class="wrap">';
	echo sprintf('<h2 class="wp-heading-inline">%s</h2>', __('Order status') );
	$obj = new order_status_list();
	$obj->prepare_items();
	$obj->display();
	echo '</div>';
}


add_action( 'woocommerce_order_status_cancelled', 'restore_product_stock_callback' );
add_action( 'woocommerce_order_status_failed', 'restore_product_stock_callback' );
add_action( 'trash_shop_order', 'restore_product_stock_callback' );
add_action( 'woocommerce_order_status_processing', 'restore_product_stock_from_cancel_to_processing_callback' );
add_action( 'woocommerce_order_status_completed', 'restore_product_stock_from_cancel_to_processing_callback' );
add_action( 'woocommerce_order_status_on-hold', 'restore_product_stock_from_cancel_to_processing_callback' );
add_action( 'publish_shop_order', 'restore_product_stock_from_cancel_to_processing_callback' );

function restore_product_stock_callback( $order_id ){
	$stock = get_post_meta( $order_id, 'update_stock', true );
	if( $stock ){
		return;	
	}
	$order = wc_get_order( $order_id );
	$items = $order->get_items();	
	foreach ( $items as $item ) {
		$product_id = $item->get_product_id();
		$product_variation_id = $item->get_variation_id();
		if( $product_variation_id ){
			$product_id = $product_variation_id;
		}
		
		$manage_stock = get_post_meta( $product_id, '_manage_stock', true );
		if( $manage_stock == 'no' ){
			$_stock_status = get_post_meta( $product_id, '_stock_status', true );
			if( $_stock_status == 'instock' ){
				return;
			}
		}
			
		$order_qty = $item->get_quantity();
		$stock = get_post_meta( $product_id, '_stock', true );
		update_post_meta( $product_id, '_stock', $order_qty + $stock );
	}
	update_post_meta( $order_id, 'update_stock', 1 );
}

function restore_product_stock_from_cancel_to_processing_callback( $order_id ){
	$stock = get_post_meta( $order_id, 'update_stock', true );
	if( $stock ){
		$order = wc_get_order( $order_id );
		$items = $order->get_items();	
		foreach ( $items as $item ) {
			$product_id = $item->get_product_id();
			$product_variation_id = $item->get_variation_id();
			if( $product_variation_id ){
				$product_id = $product_variation_id;
			}
			
			$manage_stock = get_post_meta( $product_id, '_manage_stock', true );
			if( $manage_stock == 'no' ){
				$_stock_status = get_post_meta( $product_id, '_stock_status', true );
				if( $_stock_status == 'instock' ){
					return;
				}
			}
			$order_qty = $item->get_quantity();
			$stock = get_post_meta( $product_id, '_stock', true );
			update_post_meta( $product_id, '_stock', $stock - $order_qty );
		}
		delete_post_meta( $order_id, 'update_stock', 1 );
	}
}

add_action( 'woocommerce_before_save_order_items', 'update_wc_stock_after_edit_qty', 10, 2 );
function update_wc_stock_after_edit_qty( $order_id, $items ) {
        
    $order = wc_get_order( $order_id );
	$order_items = $order->get_items();	
	foreach ( $order_items as $o_item_id=>$item ) {
		$product_id = $item->get_product_id();
		$product_variation_id = $item->get_variation_id();
		if( $product_variation_id ){
			$product_id = $product_variation_id;
		}
		$order_qty = $item->get_quantity();
		$updated_qty = $items['order_item_qty'][$o_item_id];
		$diff_qty = abs($order_qty - $updated_qty);
		$stock = get_post_meta( $product_id, '_stock', true );
				
		if( $order_qty > $updated_qty )
		{
			update_post_meta( $product_id, '_stock', $stock + $diff_qty );
		}
		else
		{
			update_post_meta( $product_id, '_stock', $stock - $diff_qty );
		}
		
	}
    
}

add_action( 'woocommerce_before_delete_order_item', 'update_stock_after_delete_order_item' ); 
function update_stock_after_delete_order_item( $item_get_id ) { 
	$variation_id = wc_get_order_item_meta($item_get_id,'_variation_id',true);
	$product_id = wc_get_order_item_meta($item_get_id,'_product_id',true);
	$order_qty = wc_get_order_item_meta($item_get_id,'_qty',true);
	
	if($variation_id) 
	{
		$product_id = $variation_id;
	}
	$stock = get_post_meta($product_id,'_stock',true);	
	
	update_post_meta( $product_id, '_stock', $stock + $order_qty );	
	
} 

add_action( 'woocommerce_ajax_add_order_item_meta', 'update_stock_after_add_order_item', 10, 2 ); 
function update_stock_after_add_order_item( $item_id, $item ) { 
	
    $variation_id = wc_get_order_item_meta($item_id,'_variation_id',true);
	$product_id = wc_get_order_item_meta($item_id,'_product_id',true);
	$order_qty = wc_get_order_item_meta($item_id,'_qty',true);
	
	if($variation_id) 
	{
		$product_id = $variation_id;
	}
	$stock = get_post_meta($product_id,'_stock',true);	
	
	update_post_meta( $product_id, '_stock', $stock - $order_qty );	
} 
     

?>
