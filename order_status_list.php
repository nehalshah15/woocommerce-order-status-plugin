<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class order_status_list extends WP_List_Table {

	public $data = array();
	public $order_ids_arr = array();
	public $product_id_name_arr = array();
	public $order_prod_qty = array();
	public $stock_prod = array();
	public $physical_stock = array();
	public $stock_status = array();
	
	public function __construct() {

		   parent::__construct( [
			  'singular' => __( 'order_list', 'os' ), 
			  'plural'   => __( 'order_lists', 'os' ), 
			  'ajax'     => false
		   ] );
		   
		   
		  //$per_page     = $this->get_items_per_page( 'orders_per_page', 100 );
		  //$current_page = $this->get_pagenum();

		  //$this->get_orders_list( $per_page, $current_page );
		  $this->get_orders_list();
	}
	
	//public function get_orders_list( $per_page = 100, $page_number = 1 ) {
	public function get_orders_list() {
		
		  global $wpdb,$woocommerce,$product;

		  $sql = 'SELECT DISTINCT WO.order_id,DATE_FORMAT(p.post_date,"%Y-%m-%d") as date,a.meta_value AS product_id,b.meta_value AS qty,a.order_item_id
				FROM '.$wpdb->prefix.'woocommerce_order_itemmeta AS a
				LEFT JOIN '.$wpdb->prefix.'woocommerce_order_itemmeta AS b ON ( a.order_item_id = b.order_item_id )
				LEFT JOIN '.$wpdb->prefix.'woocommerce_order_items AS WO ON ( WO.order_item_id = b.order_item_id )
				LEFT JOIN '.$wpdb->prefix.'posts AS p ON ( p.ID = WO.order_id )
				WHERE a.meta_key = "_product_id" AND b.meta_key = "_qty"
				AND p.post_status IN ("wc-processing","wc-pending","wc-on-hold") order by WO.order_id ASC';	
		  	
		  /*if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		  }*/
		  
		  //$sql .= ' LIMIT '.$per_page;
		  //$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		  $result = $wpdb->get_results( $sql, 'ARRAY_A' );
		    
		  $arr = array();
		  foreach($result as $oid)
		  {
		      $arr[$oid['order_id']][] = array('product_id'=>$oid['product_id'],
											 'order_item_id'=>$oid['order_item_id'],
											 'date'=>$oid['date'],
											 'qty'=>$oid['qty']
											 );
	      }
		  foreach($arr as $key=>$a_rr)
		  {	
			  $product_arr = array();
			  $order_date =	 '';
			 
			  foreach($a_rr as $a)
			  {
				 $order_date = $a['date'];
				 $product_id = $a['product_id'];
				 
				 $variation_id = wc_get_order_item_meta($a['order_item_id'],'_variation_id',true);
				 
				 if($variation_id) 
				 {
					$product_id = $variation_id;
				 }
				 
				 $product_name = get_the_title($product_id);
				 $this->product_id_name_arr[$product_id] = $product_name;
				 $product_arr[$product_id] = $a['qty'];
				 
				 if(isset($this->order_prod_qty[$product_id]))
				 {
					$this->order_prod_qty[$product_id] = $this->order_prod_qty[$product_id] + $a['qty'];	
				 }
				 else
				 {					
					$this->stock_prod[$product_id] = get_post_meta($product_id,'_stock',true);	
					$manage_stock = get_post_meta( $product_id, '_manage_stock', true );
					$status = 2;
					if( $manage_stock == 'no' ){
						$_stock_status = get_post_meta( $product_id, '_stock_status', true );
						if( $_stock_status == 'instock' ){
							$status = 1;
						}else{
							$status = 0;
						}
					}
					$this->stock_status[] = $status;	
					
					$this->order_prod_qty[$product_id] = $a['qty'];	
					
					
				 }
			  }
			  
			  $this->order_ids_arr[] = array('order_id'=>$key,
											  'date'=>$order_date,
											  'order_qty'=>$product_arr
											 );
		  }
		  foreach($this->order_prod_qty as $key=>$value)
		  {
				$this->physical_stock[$key]  = $value + $this->stock_prod[$key];
		  }
		  
		  $this->order_ids_arr[] = array('order_id'=>'Total Product Ordered','date'=>'','order_qty'=>$this->order_prod_qty, 'footer_row' => 'footer_row_first');
		  $this->order_ids_arr[] = array('order_id'=>'Woocommerce Stock Level','date'=>'','order_qty'=>$this->stock_prod, 'footer_row' => 0);
		  $this->order_ids_arr[] = array('order_id'=>'Physical Stock','date'=>'','order_qty'=>$this->physical_stock, 'footer_row' =>0);
		  //$this->product_id_name_arr = array_unique($this->product_id_name_arr);
		  $this->data = $this->order_ids_arr;
	}
	
	public function no_items() {
		  _e( 'No orders avaliable.', 'os' );
	}  
	
	public function column_default( $item, $column_name ) {
	
		switch ( $column_name ) {
			case 'date':
			  return $item[ $column_name ];
			case 'order_id':
			 if(is_numeric($item[ $column_name ]))
			 {
				 $link = sprintf('<a href="post.php?post=%s&action=edit">#%s</a>', $item[ $column_name ], $item[ $column_name ]) ;
				  return $link.'<span class="order_status_mark"></span>';
			 }
			 return $item[ $column_name ];
			default:
				if(isset($item['order_qty'][$column_name]))
				{
					return $item['order_qty'][$column_name];
				}
				return 0;
			  //return print_r( $item, true );
		  }
	}
	
	public function display() {
			
			$singular = $this->_args['singular'];

			$this->display_tablenav( 'top' );

			$this->screen->render_screen_reader_content( 'heading_list' );
	?>
	<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
		<thead>
		<tr>
			<?php $this->print_column_headers(); ?>
		</tr>
		</thead>

		<tbody id="the-list"<?php
			if ( $singular ) {
				echo " data-wp-lists='list:$singular'";
			} ?>>
			<?php $this->display_rows_or_placeholder(); ?>
		</tbody>


	</table>
	<input type="hidden" id="product_status" value='<?php echo json_encode($this->stock_status); ?>' />
	<?php
			$this->display_tablenav( 'bottom' );
	}
	
	function get_columns() {
		  		  
		  $columns = array('date' => __( 'Date', 'os' ),
			'order_id' => __( 'Order Id', 'os' )
  		  );
			
		  $pro_id_arr = $this->product_id_name_arr;
		  foreach($pro_id_arr as $key=>$prod_arr)
		  {
				$columns[$key] = $prod_arr;	
		  }
		  
		  return $columns;
	}
	
	/*public function get_sortable_columns() {
		  $sortable_columns = array(
			'date' => array( 'date', true ),
			'order_id' => array( 'order_id', false )
		  );
		  
		  return $sortable_columns;
    }*/
	
	public function prepare_items() {

		  $this->_column_headers = array($this->get_columns());

		  $per_page     = $this->get_items_per_page( 'orders_per_page', 100 );
		  $current_page = $this->get_pagenum();
		  /*$total_items  = self::record_count();

		  $this->set_pagination_args( [
			'total_items' => $total_items,
			'per_page'    => $per_page
		  ] );*/

		  $this->items = $this->data;
		  	  
	}
	
	public function single_row( $item ){
		$class = '';
		if( isset( $item['footer_row'] ) ){
			$class = 'footer_row '. ( $item['footer_row'] ? $item['footer_row'] : '' );
		}
		echo '<tr class="'.$class.'">';
		$this->single_row_columns( $item );
		echo '</tr>';
	}
	
	protected function single_row_columns( $item ) {
                list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();
                foreach ( $columns as $column_name => $column_display_name ) {
                        $classes = "$column_name column-$column_name";
                        if ( $primary === $column_name ) {
                                $classes .= ' has-row-actions column-primary';
                        }
                        if ( in_array( $column_name, $hidden ) ) {
                                $classes .= ' hidden';
                        }
                        if( isset( $item['footer_row'] ) ){
							if ( 'date' === $column_name ) {
								$classes .= ' hidden';
							}
						}
                        // Comments column uses HTML in the display name with screen reader text.
                        // Instead of using esc_attr(), we strip tags to get closer to a user-friendly string.
                        $data = 'data-colname="' . wp_strip_all_tags( $column_display_name ) . '"';
                        $attributes = "class='$classes' $data";
                        if( isset( $item['footer_row'] ) ){
							if ( 'order_id' === $column_name ) {
								$attributes .=  ' colspan=2';
							}
						}
                        if ( 'cb' === $column_name ) {
                                echo '<th scope="row" class="check-column">';
                                echo $this->column_cb( $item );
                                echo '</th>';
                        } elseif ( method_exists( $this, '_column_' . $column_name ) ) {
                                echo call_user_func(
                                        array( $this, '_column_' . $column_name ),
                                        $item,
                                        $classes,
                                        $data,
                                        $primary
                                );
                        } elseif ( method_exists( $this, 'column_' . $column_name ) ) {
                                echo "<td $attributes>";
                                echo call_user_func( array( $this, 'column_' . $column_name ), $item );
                                echo $this->handle_row_actions( $item, $column_name, $primary );
                                echo "</td>";
                        } else {
                                echo "<td $attributes>";
                                echo $this->column_default( $item, $column_name );
                                echo $this->handle_row_actions( $item, $column_name, $primary );
                                echo "</td>";
                        }
                }
        }
	
}

?>
